﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaModel;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;

namespace MyValorDataGen
{
    class Program
    {
        static void Main(string[] args)
        {
            //Generate the data
            GenerateRecord dataGen = new GenerateRecord();
            ModelTranslation translator = new ModelTranslation();

            Model.RootObject allData = dataGen.getModel(8);
            StudentRecord.RootObject records = translator.toParentDirRecord(allData);

            //Post the data
            postRequest(records);
            
        }

        static void postRequest(StudentRecord.RootObject root)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMGetParentStudentDirectory");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(root), ParameterType.RequestBody);

            // easy async support
            //client.ExecuteAsync(request, response =>
            //{
            //    Console.WriteLine(response.Content);
            //});
            //Console.ReadLine();

            client.Execute(request);
        }
    }
}
